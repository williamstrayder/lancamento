import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Lancamento from "../../components/Lancamento";
import Footer from "../../components/Footer";
const useStyles = makeStyles((theme) => ({
  root: {
    // background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  }
}));

const Home = () => {

  const classes = useStyles();
  return (
    <div className={classes.root}>
     <Lancamento />
     <Footer />
    </div>
  );
};

export default Home;
