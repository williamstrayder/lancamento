import React, { useState } from "react";
import {
  Button,
  Container,
  TextField,
  Slide,
  Snackbar,
  LinearProgress,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import useStyles from "./styles";
import axios from "axios";

const initValues = {
  nome: "",
  telefone: "",
  email: "",
};

const Lancamento = () => {
  const [values, setValues] = useState(initValues);
  const [load, setLoad] = useState(false);
  const [state, setState] = useState({
    text: "Obrigado por se cadastrar!",
    severity: "success",
    open: false,
    Transition: SlideTransition,
  });
  const onChange = (event) => {
    console.log(values);
    const { name, value } = event.target;
    setValues({ ...values, [name]: value });
  };
  const cleanAll = () => {
    setValues({ nome: "", telefone: "", email: "" });
  };
  function SlideTransition(props) {
    return <Slide {...props} direction="up" />;
  }

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  const openSnackbars = ({ text, severity }) => {
    console.log("entrou openSnackbars");
    setState({
      text,
      severity,
      open: true,
      Transition: SlideTransition,
    });
  };
  const handleClose = () => {
    setState({
      ...state,
      open: false,
    });
  };
  const enviarForm = (event) => {
    const url = process.env.REACT_APP_HOST;

    // if (!validarFormulario()) {
    //   return false;
    // }
    // if (!validarPassword()) {
    //   return false;
    // }

    setLoad(true);
    let params = values;
    axios
      .post(url, params)
      .then(() => {
        console.log("enviou");
        cleanAll();
        openSnackbars({
          text: "Obrigado por se cadastrar!",
          severity: "success",
        });
        // setValidar(false);
        setLoad(false);
      })
      .catch((error) => {
        setLoad(false);
        // Handle error.
        if (error) {
          console.log(error);
        }
        // if (error.response) {
        //   const erros = error.response.data.data[0].messages;
        //   erros.forEach((element) => {
        //     console.log(element);
        //     if (element.id === "Auth.form.error.email.taken") {
        //       openSnackbars({
        //         text: "Email já utilizado!",
        //         severity: "error",
        //       });
        //     }
        //     if (element.id === "Auth.form.error.email.format") {
        //       openSnackbars({
        //         text: "Forneça um endereço de e-mail válido.",
        //         severity: "error",
        //       });
        //     }
        //   });
        // }
      });
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      {load && <LinearProgress className={classes.linearProgress} />}
      <Container className={classes.container}>
        <h1 className={classes.title}>Cadastre-se para o lançamento.</h1>
        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            id="nome"
            label="Nome"
            variant="filled"
            className={classes.input}
            name="nome"
            value={values.username}
            onChange={onChange}
          />
          <TextField
            id="telefone"
            label="Telefone"
            value={values.telefone}
            name="telefone"
            onChange={onChange}
            variant="filled"
            className={classes.input}
          />
          <TextField
            id="email"
            label="E-mail"
            variant="filled"
            className={classes.input}
            value={values.email}
            name="email"
            onChange={onChange}
          />
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={enviarForm}
          >
            Cadastre-se
          </Button>
        </form>
      </Container>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={state.open}
        autoHideDuration={6000}
        onClose={handleClose}
        key={"bottomright"}
      >
        <Alert onClose={handleClose} severity={state.severity}>
          {state.text}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default Lancamento;
