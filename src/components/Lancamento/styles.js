import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    // color: theme.palette.primary.contrastText,
    // minHeight: "calc( 100vh)",
    // position: "relative",
    // background: "rgba(255,255,255,1)",
  },
  container:{
    marginTop: '40px',
    marginBottom: '60px',
  },
  title:{
    margin: '30px auto',
    width: 'fit-content',
    color: theme.palette.primary.dark,
    fontFamily: 'Poppins, Sans-serif',
    fontWeight: 600,
    textTransform: 'uppercase',
    // fontSize: '60px',
    fontSize: '45px',
  },
  form: {
    backgroundColor: theme.palette.primary.light,
    display: "flex",
    flexDirection: "column",
    padding: "10px",
    borderRadius: 3,
    maxWidth: "400px",
    margin: 'auto',
    "& * > .MuiInputBase-root.MuiFilledInput-root.MuiFilledInput-underline.MuiInputBase-formControl":
      {
        backgroundColor: "#ffffffd4",
      },
  },
  input: {
    margin: "5px",
  },
  button: {
    maxWidth: "260px",
    margin: "auto",
    color: theme.palette.primary.contrastText
  },
}));

export default useStyles;
