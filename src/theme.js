import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#4ec17e',
      main: '#009051',
      dark: '#006127',
      contrastText: '#000000',
    },
    secondary: {
      light: '#d1ffff',
      main: '#9de1fe',
      dark: '#6bafcb',
      contrastText: '#ffffff',
    },
  },
});

export default theme;
